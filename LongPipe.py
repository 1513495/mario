import pygame

from const import *
from functions import *
from Mario import MARIO

SIZEX = PIPE_SIZEX
SIZEY = 60

PATH = "assets/images/LongPipe/1.png"

class LongPipe(pygame.sprite.Sprite):
    def __init__(self, centerx, centery):
        super(LongPipe, self).__init__()

        # Initialize sprites
        self.image = pygame.image.load(PATH)

        self.rect = pygame.Rect(centerx - SIZEX / 2, centery - SIZEY / 2, SIZEX, SIZEY)

    def update(self):
        pass

    def handle(self, pipes):
        keys = pygame.key.get_pressed()

        # Moving pipe along background
        if keys[pygame.K_RIGHT] and MARIO.touchPipeLeft(pipes) is False:
            self.rect.centerx -= SPEED_DEFAULT
        elif keys[pygame.K_LEFT] and MARIO.touchPipeRight(pipes) is False:
            self.rect.centerx += SPEED_DEFAULT

    # Check if something touch pipe
    def touch(self, rect):
        horizontal = abs(self.rect.centerx - rect.right) <= SIZEX / 2 or abs(self.rect.centerx - rect.left) <= SIZEX / 2
        upperVertical = rect.bottom < self.rect.top
        bottomVertical = rect.top > self.rect.bottom + 11
        vertical = not (upperVertical or bottomVertical)
        # print (str(upperVertical) + "--" + str(rect.top) + "--" + str(self.rect.bottom))
        return horizontal and vertical

    # Check if something touch pipe to the left
    def touchLeft(self, rect):
        horizontal = self.rect.centerx - rect.right <= SIZEX / 2 and self.rect.centerx > rect.right
        upperVertical = rect.bottom < self.rect.top
        bottomVertical = rect.top > self.rect.bottom + 11
        vertical = not (upperVertical or bottomVertical)
        # print (str(upperVertical) + "--" + str(rect.top) + "--" + str(self.rect.bottom))
        return horizontal and vertical

    # Check if MARIO touch pipe to the right
    def touchRight(self, rect):
        horizontal = rect.left - self.rect.centerx <= SIZEX / 2 and rect.left > self.rect.centerx
        upperVertical = rect.bottom < self.rect.top
        bottomVertical = rect.top > self.rect.bottom + 11
        vertical = not (upperVertical or bottomVertical)
        # print (str(upperVertical) + "--" + str(rect.top) + "--" + str(self.rect.bottom))
        return horizontal and vertical
