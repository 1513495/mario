class InfoJump:
  def __init__(self):
    self.index = 0

    self.jumps = generateJumps([7, 7, 6, 6, 5, 3, 1, 0, -1, -1, -2, -3, -4, -6, -8, -10])
    self.idxMax = len(self.jumps)

def generateJumps(arr):
  jumps = []

  for i in arr:
    for j in range(4):
      jumps.append(i)

  return jumps