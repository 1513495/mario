import pygame
import os
import time
from random import randint

from const import *
from functions import *
from InfoJump import InfoJump
from Mario import MARIO

DISTANCE_NEW_FRAME = 18

COIN_IMAGES = load_PNG_Images("assets/images/coin/")

class Obj(pygame.sprite.Sprite):
  def __init__(self, left, bottom, path):
    super(Obj, self).__init__()

    self.index = 1

    # load images for sprites
    self.images = COIN_IMAGES

    self.image = self.images[0]

    self.rect = pygame.Rect(left, bottom - self.image.get_height() + 20, self.image.get_width(), self.image.get_height())

  def update(self):
    # check dead
    if self.rect.centerx < -100:
      self.kill()

  def move(self, direction, speed = SPEED_DEFAULT):
    # update new frame
    if direction == RIGHT:
      self.rect = self.rect.move(speed, 0)
    elif direction == LEFT:
      self.rect = self.rect.move(-speed, 0)

  def handle(self, speed = SPEED_DEFAULT):
    keys = pygame.key.get_pressed()  # take pressed keys

    # moving follow mario
    if keys[pygame.K_LEFT]:
      self.move(RIGHT)
    elif keys[pygame.K_RIGHT]:
      self.move(LEFT)

class Coin(Obj):
  def __init__(self, left, bottom):
    super(Coin, self).__init__(left, bottom, "assets/images/coin/")
    self.isDead = False
    self.index = 0
    self.numFrames = 0
  
  def handle(self):
    super().handle()
    if self.isDead:
      self.runAnimation()
      return
    
    if self.rect.colliderect(MARIO.rect):
      MARIO.gold += 1
      self.isDead = True
    
    if self.rect.centerx < MIN_MIN:
      self.kill()
  
  def runAnimation(self):
    if self.numFrames > 8:
      self.numFrames = 0

      self.index += 1

      if self.index >= len(self.images) - 1:
        self.kill()
      self.image = self.images[self.index]
      self.rect = pygame.Rect(self.rect.centerx - int(self.image.get_width() / 2), self.rect.bottom - self.image.get_height(), self.image.get_width(), self.image.get_height())
    else:
      self.numFrames += 1

class SpriteGroupCoin(pygame.sprite.Group):
  def __init__(self):
    super(SpriteGroupCoin, self).__init__()

  def handle(self):
    for sprite in self.sprites():
      sprite.handle()

    if len(self.sprites()) == 0:
      self.addCoin()

  def addCoin(self):
    number = randint(1000, 1800)
    for i in range(5):
      self.add(Coin(number + 50 * i, 460))

SpriteGroupCoin = SpriteGroupCoin()