import pygame
import colors
from const import *
from functions import *
from Mario import MARIO
pygame.init()

BGPATH = 'assets/images/background/'
SKYPATH = 'assets/images/skyBg/'

GROUND_IMG_WIDTH = 1280
GROUND_IMG_HEIGHT = BACKGROUND_HEIGHT

SKY_IMG_WIDTH = 2560


class Background(pygame.Surface):
  def __init__(self):
    super(Background, self).__init__((WINDOW_WIDTH, WINDOW_HEIGHT))
    self.fill(colors.Black)

class GroundSprite(pygame.sprite.Sprite):
  def __init__(self, x, y):
    super(GroundSprite, self).__init__()
    self.sprites = []

    for img in load_PNG_Images(BGPATH):
      self.sprites.append(img)

    self.image = self.sprites[0]
    self.position = [x, y]
    self.rect = pygame.Rect(self.position[0], self.position[1], GROUND_IMG_WIDTH, GROUND_IMG_HEIGHT)

  def update(self, pipes):
    self.disappear()
    keys = pygame.key.get_pressed()  # take pressed keys

    # moving
    if keys[pygame.K_LEFT] and MARIO.touchPipeRight(pipes) is False:
      self.position[0] += SPEED_DEFAULT
    if keys[pygame.K_RIGHT] and MARIO.touchPipeLeft(pipes) is False:
      self.position[0] -= SPEED_DEFAULT
    self.rect = pygame.Rect(self.position[0], self.position[1], SKY_IMG_WIDTH, GROUND_IMG_HEIGHT)

  def disappear(self):
    # print(self.position[0], " - ", self.position[0] + WINDOW_WIDTH)

    if self.position[0] + GROUND_IMG_WIDTH < 0:
      self.kill()

      pass
class SkySprite(pygame.sprite.Sprite):
  def __init__(self, x, y):
    super(SkySprite, self).__init__()
    self.sprites = []

    for img in load_PNG_Images(SKYPATH):
      self.sprites.append(img)

    self.image = self.sprites[0]
    self.position = [x, y]
    self.rect = pygame.Rect(0, 0, SKY_IMG_WIDTH, GROUND_IMG_HEIGHT)

  def update(self, pipes):
    self.disappear()
    keys = pygame.key.get_pressed()  # take pressed keys

    # update direction
    if keys[pygame.K_LEFT] and MARIO.touchPipeRight(pipes) is False:
      self.position[0] += SPEED_DEFAULT
    if keys[pygame.K_RIGHT] and MARIO.touchPipeLeft(pipes) is False:
      self.position[0] -= SPEED_DEFAULT
    self.rect = pygame.Rect(self.position[0], self.position[1], SKY_IMG_WIDTH, GROUND_IMG_HEIGHT)
  def disappear(self):
    # print(self.position[0], " - ", self.position[0] + WINDOW_WIDTH)
    if self.position[0] + SKY_IMG_WIDTH < 0:
      self.kill()
