import pygame

from const import *
from functions import *
from Mario import MARIO

SIZEX = 40
SIZEY = 60
DISTANCE_NEW_FRAME = 0
MOVE_SPEED = 3
SURF_SPEED = 6

PATH = "assets/images/BlueTurtle/"

class BlueTurtle(pygame.sprite.Sprite):
    def __init__(self, centerx, centery):
        super(BlueTurtle, self).__init__()

        # Initialize sprites
        self.sprites = {
            LEFT: [],
            RIGHT: [],
            DRABBLED: [],
            SURF_LEFT: [],
            SURF_RIGHT: [],
            DIE: []
        }
        # Load sprites
        loadImages(self.sprites[LEFT], PATH + "left/", SIZEX, SIZEY)
        loadImages(self.sprites[RIGHT], PATH + "right/", SIZEX, SIZEY)
        loadImages(self.sprites[DRABBLED], PATH + "drabbled/", 70, 70)
        loadImages(self.sprites[SURF_LEFT], PATH + "surf/", 70, 70)
        loadImages(self.sprites[SURF_RIGHT], PATH + "surf/", 70, 70)
        loadImages(self.sprites[DIE], PATH + "die/", 70, 70)

        self.rect = pygame.Rect(centerx - SIZEX / 2, centery - SIZEY / 2, SIZEX, SIZEY)
        self.index = 1
        self.direction = LEFT
        images = self.sprites[self.direction]
        self.image = images[self.index]
        self.totalDistance = 0

        # Accessible time
        self.availableCount = 0
        # Time to be surf after being drabbled
        self.surfCountDown = 0
        # Time to be re-calculated after turtles reflection
        self.reCalcTurtleCount = 0

    def update(self):
        pass

    def move(self, direction, pipes):
        self.direction = direction
        images = self.sprites[direction]

        # Calculate speed based on background speed
        keys = pygame.key.get_pressed()
        backgroundSpeed = calcBGSpeed(keys, self.direction, pipes)

        if self.totalDistance > DISTANCE_NEW_FRAME:
            if self.direction is LEFT:
                self.rect.centerx -= MOVE_SPEED + backgroundSpeed
            elif self.direction is RIGHT:
                self.rect.centerx += MOVE_SPEED + backgroundSpeed
            elif self.direction is SURF_LEFT:
                self.rect.centerx -= SURF_SPEED + backgroundSpeed
            elif self.direction is SURF_RIGHT:
                self.rect.centerx += SURF_SPEED + backgroundSpeed
            self.image = images[self.index]
            self.totalDistance = 0
            self.index += 1

        if self.index >= len(images):
            self.index = 0

        self.totalDistance += 1

    def handle(self, pipes, turtles):
        self.move(self.direction, pipes)
        # This turtle cannot be touched if availableCount > 0
        self.availableCount -= 1
        if (self.availableCount > 0):
            return
        # This turtle will be surfing after this comes smaller than zero
        self.surfCountDown -= 1
        # This turtle will be calculated reflection with other turtles after this count down
        self.reCalcTurtleCount -= 1

        if self.direction is DIE:
            self.kill()

        if self.direction is DRABBLED:
            if self.availableCount < 1:
                self.availableCount = 0
                self.direction = SURF_RIGHT

        # Check colliction with mario
        self.collection_mario()

        # Handle reflection with pipes
        self.handlePipeReflect(pipes)
        # Handle reflection with other turtles
        self.handleTurtleReflect(turtles)

    def collection_mario(self):
        if self.rect.colliderect(MARIO):
            if self.isDrabbledByMario() and self.direction  not in [DRABBLED, SURF_LEFT, SURF_RIGHT]:
                self.direction = DRABBLED
                self.surfCountDown = 60
            else:
                if MARIO.isGiant and self.surfCountDown < 1:
                    self.dying()
                elif MARIO.state == State.JUMPING and MARIO.infoJump.index < MARIO.infoJump.idxMax and MARIO.infoJump.jumps[MARIO.infoJump.index] < 0 and self.surfCountDown < 1:
                    self.dying()
                elif self.surfCountDown < 1:
                    MARIO.die()
    # Check if Mario has drabbled on turtle
    # This was used after colliderect(MARIO) so just need to check if MARIO is above
    def isDrabbledByMario(self):
        isRightAbove = abs(MARIO.rect.bottom - self.rect.top) < 10
        return isRightAbove

    # Kill this turtle
    def dying(self):
        self.direction = DIE
        self.availableCount = 20
        MARIO.gold += 1
        # After 20 counts, this object will be killed

    # Handle reflection with pipes
    def handlePipeReflect(self, pipes):
        for pipe in pipes:
            if pipe.touch(self.rect):
                if self.direction is LEFT:
                    self.direction = RIGHT
                    self.availableCount = 5
                elif self.direction is RIGHT:
                    self.direction = LEFT
                    self.availableCount = 5
                elif self.direction is SURF_LEFT:
                    self.direction = SURF_RIGHT
                    self.availableCount = 5
                elif self.direction is SURF_RIGHT:
                    self.direction = SURF_LEFT
                    self.availableCount = 5

    # Handler reflection with other turtles
    def handleTurtleReflect(self, turtles):
        for turtle in turtles:
            if turtle is not self and self.touch(turtle.rect) is True:
                interactTurtles(self, turtle)

    # Check if this turtle touches something
    def touch(self, rect):
        horizontal = abs(self.rect.centerx - rect.right) <= SIZEX / 2 or abs(self.rect.centerx - rect.left) <= SIZEX / 2
        return horizontal

def interactTurtles(turtle1, turtle2):
    if turtle1.reCalcTurtleCount > 0 or turtle2.reCalcTurtleCount > 0:
        return
    if turtle1.direction is DIE and turtle2.direction is DIE:
        return
    if turtle1.direction is LEFT and turtle2.direction is RIGHT:
        turtle1.direction = RIGHT
        turtle2.direction = LEFT
    elif turtle1.direction is RIGHT and turtle2.direction is LEFT:
        turtle1.direction = LEFT
        turtle2.direction = RIGHT
    elif turtle1.direction in [SURF_LEFT, SURF_RIGHT] and turtle2.direction in [LEFT, RIGHT]:
        turtle2.dying()
    elif turtle2.direction in [SURF_LEFT, SURF_RIGHT] and turtle1.direction in [LEFT, RIGHT]:
        turtle1.dying()
    elif turtle1.direction in [SURF_LEFT, SURF_RIGHT] and turtle2.direction in [SURF_LEFT, SURF_RIGHT]:
        turtle1.dying()
        turtle2.dying()
    turtle1.reCalcTurtleCount = 10
    turtle2.reCalcTurtleCount = 10


def loadImages(list, path, scaledSizeX, scaledSizeY):
	for filename in os.listdir(path):
		if filename.endswith(".png"):
			image = pygame.image.load(path + filename)
			resizeImage = pygame.transform.scale(image, (scaledSizeX, scaledSizeY))
			list.append(resizeImage)

def calcBGSpeed(keys, moveDirection, pipes):
    if keys[pygame.K_LEFT] and MARIO.touchPipeRight(pipes) is False:
        if moveDirection is LEFT or moveDirection is SURF_LEFT:
            return -1 * SPEED_DEFAULT
        else:
            return 1 * SPEED_DEFAULT
    if keys[pygame.K_RIGHT] and MARIO.touchPipeLeft(pipes) is False:
        if moveDirection is RIGHT or moveDirection is SURF_RIGHT:
            return -1 * SPEED_DEFAULT
        else:
            return 1 * SPEED_DEFAULT
    return 0
