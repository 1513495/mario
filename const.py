import pygame

from enum import Enum

pygame.init()

UP = 'UP'
DOWN = 'DOWN'
RIGHT = 'RIGHT'
LEFT = 'LEFT'
UP_LEFT = 'UP_LEFT'
UP_RIGHT = 'UP_RIGHT'
DOWN_LEFT = 'DOWN_LEFT'
DOWN_RIGHT = 'DOWN_RIGHT'
RUNNING = 'RUNNING'
JUMPING = 'JUMPING'
STANDING = 'STANDING'

DRABBLED = "DRABBLED"
SURF_LEFT = "SURF_LEFT"
SURF_RIGHT = "SURF_RIGHT"
DIE  = "DIE"

MARIO_SIZE = 100

PIPE_SIZEX = 40

SQRT_2 = 1.4142

FPS = 60

WINDOW_WIDTH = 1000
WINDOW_HEIGHT = 600

BACKGROUND_HEIGHT = 200
GROUND_HEIGHT = 136

GAME_WIDTH = WINDOW_WIDTH
GAME_HEIGHT = 600

TABLE_SCORE_WIDTH = WINDOW_WIDTH
TABLE_SCORE_HEIGHT = WINDOW_HEIGHT - GAME_HEIGHT

RECT_GAME = pygame.Rect(0, 0, GAME_WIDTH, GAME_HEIGHT)

BORDER_WIDTH = 1

SIZE_ITEM = 40
TEXT_WIDTH = 2

WIDTH_OF_PLAY_AGAIN_BOARDING = WINDOW_WIDTH / 2
HEIGHT_OF_PLAY_AGAIN_BOARDIG = WINDOW_HEIGHT / 2

WIDTH_OF_PAUSE_GAME = WINDOW_WIDTH / 3
HEIGHT_OF_PAUSE_GAME = WINDOW_HEIGHT / 2

FRAMS_TO_RERENDER_NEW_OBJECT = 50

WITH_SETTING_BOARD = WINDOW_WIDTH / 3
HEIGHT_SETTING_BOARD = WINDOW_HEIGHT

SPEED_DEFAULT = 3

class State(Enum):
  FREE = 1
  RUNNING = 2
  JUMPING = 3

FONT30 = pygame.font.Font('assets/fonts/Muli-Regular.ttf', 30)
FONT30B = pygame.font.Font('assets/fonts/Muli-Bold.ttf', 30)
FONT60B = pygame.font.Font('assets/fonts/Muli-Bold.ttf', 60)

MIN_MIN = -1000
MAX_MAX = 2000
