import pygame
import os
import time

from const import *
from functions import *
from Mario import MARIO

SIZE = 32
DISTANCE_NEW_FRAME = 18

PATH = "assets/images/Mushroom/"

class Mushroom(pygame.sprite.Sprite):
    def __init__(self, left, bottom):
        super(Mushroom, self).__init__()

        self.index = 1

        self.totalDistance = 0

        # load images for sprites
        self.images = loadImages()

        self.image = self.images[0]
        self.rect = pygame.Rect(left, bottom - self.image.get_height() + 40, self.image.get_width(), self.image.get_height())

    def update(self):
        # check dead
        if self.rect.centerx < -100:
            self.kill()

    def move(self, direction, speed=SPEED_DEFAULT):
        # update new frame
        if self.totalDistance > DISTANCE_NEW_FRAME:
            self.image = self.images[self.index]
            self.totalDistance = 0
            self.index += 1

        if self.index >= len(self.images):
            self.index = 0

        self.totalDistance += speed

        if direction is LEFT:
            self.rect = self.rect.move(-speed, 0)

    def handle(self):
        self.move(LEFT)
        if self.rect.colliderect(MARIO):
            MARIO.toGiant()
            self.kill()

def loadImages():
    lst = []
    for filename in os.listdir(PATH):
        if filename.endswith(".png"):
            image = pygame.image.load(PATH + filename)

            newWidth = int(image.get_rect().width * 2)
            newHeight = int(image.get_rect().height * 2)

            resizeImage = pygame.transform.scale(image, (newWidth, newHeight))
            lst.append(resizeImage)
    return lst

MUSHROOM = Mushroom(WINDOW_WIDTH - GROUND_HEIGHT,460)


class SpriteGroupMushroom(pygame.sprite.Group):
	def __init__(self):
		super(SpriteGroupMushroom, self).__init__()
	def handle(self):
		print(self.sprites)
		if len(self.sprites()) == 0:
			self.addMushroom()

	def addMushroom(self):
		number = random.randint(1000, 1800)
		self.add(Mushroom(number, 460))


SpriteGroupMushroom = SpriteGroupMushroom()