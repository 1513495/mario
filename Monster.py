import pygame
import os
import time

from const import *
from functions import *
from InfoJump import InfoJump
from Mario import MARIO

DISTANCE_NEW_FRAME = 18

TURTLE_SPEED = 1

TURTLE_IMAGES = load_PNG_Images("assets/images/turtle/", 0.5)

class Monster(pygame.sprite.Sprite):
  def __init__(self, left, bottom):
    super(Monster, self).__init__()

    self.numFrames = 0
    self.index = 0

    # load images for sprites
    self.images = TURTLE_IMAGES
    self.image = self.images[0]

    self.rect = pygame.Rect(left, bottom - self.image.get_height() + 20, self.image.get_width(), self.image.get_height())

  def update(self):
    # check dead
    if self.rect.centerx < MIN_MIN:
      self.kill()

    keys = pygame.key.get_pressed()  # take pressed keys
    # moving along mario
    if keys[pygame.K_LEFT]:
      self.move(RIGHT)
    elif keys[pygame.K_RIGHT]:
      self.move(LEFT)

    # check colliction with mario
    if self.rect.colliderect(MARIO):
      if MARIO.isGiant:
        print("I am killed by mario")

        self.kill()
      elif MARIO.state == State.JUMPING and MARIO.infoJump.index < MARIO.infoJump.idxMax and MARIO.infoJump.jumps[MARIO.infoJump.index] < 0:
        print("I am killed by mario")

        self.kill()
      else:
        MARIO.die()

    self.move(LEFT, TURTLE_SPEED)

    self.updateAnimation()

  def move(self, direction, speed = SPEED_DEFAULT):
    if direction == LEFT:
      self.rect.centerx -= speed
    elif direction == RIGHT:
      self.rect.centerx += speed

  def updateAnimation(self):
    if self.numFrames > 10:
      self.numFrames = 0

      self.image = self.images[self.index]
      self.index += 1

      if self.index >= len(self.images):
        self.index = 0
    else:
      self.numFrames += 1
  
class SpriteGroupMonster(pygame.sprite.Group):
  def __init__(self):
    super(SpriteGroupMonster, self).__init__()

  def handle(self):
    if len(self.sprites()) == 0:
      self.addMonster()

  def addMonster(self):
    number = random.randint(1000, 1800)
    self.add(Monster(number, 460))

SpriteGroupMonster = SpriteGroupMonster()