import pygame
import os
import time

from const import *
from functions import *
from InfoJump import InfoJump
# from main import newGameScreen
SIZE = MARIO_SIZE
DISTANCE_NEW_FRAME = 18
MAX_FRAME = 10
DEFAULT_EFFECT_TIME = 20

MIN_X = 120
MAX_X = WINDOW_WIDTH - MIN_X

PATH = "assets/images/mario/"

class Mario(pygame.sprite.Sprite):
  def __init__(self, left, bottom):
    super(Mario, self).__init__()

    self.index = 0
    self.numFrames = 0
    self.isEatMushRoom = False
    self.sprites = {
      RUNNING: loadImages(PATH + '/running/'),
      JUMPING: loadImages(PATH + '/jumping/'),
      STANDING: loadImages(PATH + '/standing/'),
    }

    self.heart = 3
    self.gold = 0

    self.isGiant = False
    self.isTiny = False
    self.effectTime = 0


    self.direction = RIGHT

    self.images = self.sprites[STANDING]
    self.setImage()

    self.rect = pygame.Rect(left, bottom - self.image.get_height() + 20, self.image.get_width(), self.image.get_height())

    # write for mario     NEW NEW NEW
    self.state = State.FREE

    self.infoJump = InfoJump()

  def setState(self, state):
    if state == State.RUNNING:
      self.state = state
      self.images = self.sprites[RUNNING]
      self.index = 0
      self.setImage()
      self.numFrames = 0
    elif state == State.JUMPING:
      self.state = state
      self.images = self.sprites[JUMPING]
      self.index = 0
      self.setImage()
      self.numFrames = 0
    elif state == State.FREE:
      self.state = state
      self.images = self.sprites[STANDING]
      self.index = 0
      self.setImage()
      self.numFrames = 0

  def update(self):
    if self.state == State.JUMPING:
      if self.infoJump.index < self.infoJump.idxMax:
        self.move(UP, self.infoJump.jumps[self.infoJump.index])
        self.infoJump.index += 1

        # update sprite
        self.updateImage()
      else:
        self.setState(State.FREE)
        self.infoJump.index = 0
    elif self.state == State.RUNNING:
      # update sprite
      self.updateImage()

      if self.direction == LEFT:
        self.move(LEFT)

    # return normal mario
    if self.isGiant:
      if self.effectTime < 0:
        self.isGiant = False
      else:
        self.effectTime -= 1
    elif self.isTiny:
      if self.effectTime < 0:
        self.isTiny = False
      else:
        self.effectTime -= 1

    self.rect = pygame.Rect(self.rect.centerx - int(self.image.get_width() / 2), self.rect.bottom - self.image.get_height(), self.image.get_width(), self.image.get_height())

  def move(self, direction, speed = SPEED_DEFAULT):
    if direction is UP:
      self.rect = self.rect.move(0, -speed)
    elif direction is LEFT:
      # self.rect = self.rect.move(-speed, 0)
      pass
    elif direction is RIGHT and self.rect.right < MAX_X:
      pass
      # self.rect = self.rect.move(speed, 0)

  def jump(self):
    self.setState(State.JUMPING)

  def die(self):
    print('call function die')
    self.heart -= 1

  def handle(self):
    keys = pygame.key.get_pressed()  # take pressed keys

    if keys[pygame.K_RIGHT]:
      self.updateDirection(RIGHT)
      self.move(RIGHT)

      if self.state == State.FREE:
        self.setState(State.RUNNING)
    elif keys[pygame.K_LEFT]:
      self.updateDirection(LEFT)
      self.move(LEFT)

      if self.state == State.FREE:
        self.setState(State.RUNNING)

  def updateImage(self):
    if self.numFrames > MAX_FRAME:
      self.numFrames = 0

      self.setImage()
      self.index += 1

      if self.index >= len(self.images):
        self.index = 0
    else:
      self.numFrames += 1

  def updateDirection(self, direction):
    if self.direction != direction:
      self.direction = direction
      self.setImage()
    pass

  def setImage(self):
    self.image = self.images[self.index]

    if self.direction == LEFT:
      self.image = pygame.transform.flip(self.image, True, False)

    if self.isGiant:
      self.image = pygame.transform.scale2x(self.image)

    if self.isTiny:
      newWidth = int(self.image.get_width() / 3)
      newHeight = int(self.image.get_height() / 3)

      self.image = pygame.transform.scale(self.image, (newWidth, newHeight))

  def toGiant(self, seconds = DEFAULT_EFFECT_TIME):
    self.isGiant = True
    self.isTiny = False
    self.effectTime = seconds * 10

  def toTiny(self, seconds = DEFAULT_EFFECT_TIME):
    self.isGiant = False
    self.isTiny = True
    self.effectTime = seconds * 10

  # Check if MARIO touches pipe
  def touchPipe(self, pipes):
    for pipe in pipes:
      if pipe.touch(self.rect):
        return True
    return False

  # Check if MARIO touch pipe to the left
  def touchPipeLeft(self, pipes):
    for pipe in pipes:
      if pipe.touchLeft(self.rect):
        return True
    return False

  # Check if MARIO touch pipe to the right
  def touchPipeRight(self, pipes):
    for pipe in pipes:
      if pipe.touchRight(self.rect):
        return True
    return False

def loadImages(path):
  lst = []
  for filename in os.listdir(path):
    if filename.endswith(".png"):
      image = pygame.image.load(path + filename)
      lst.append(image)
  return lst

MARIO = Mario(int(WINDOW_WIDTH / 3), WINDOW_HEIGHT - GROUND_HEIGHT)
