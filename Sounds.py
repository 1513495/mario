import os
import pygame
import random

pygame.init()

PATH = "assets/sounds/"

class Sounds:
	def __init__(self):
		for dirs in os.listdir(PATH):
			for filesound in os.listdir(PATH + dirs):
				if filesound.endswith(".wav"):
					if(dirs == "music_background"): 
						self.music_background = PATH + "/" + dirs + "/" + filesound
					if(dirs == "coin"):
						self.coin = PATH + "/" + dirs + "/" + filesound
					if(dirs == "jump"):
						self.jump = PATH + "/" + dirs + "/" + filesound
					if(dirs == "growing"):
						self.growing = PATH + "/" + dirs + "/" + filesound

	def bgMusic(self):
		pygame.mixer.music.load(self.music_background)
		pygame.mixer.music.play(-1)
		pygame.mixer.music.set_volume(0.6)

	def coinSound(self):
		sound = pygame.mixer.Sound(self.coin)
		sound.set_volume(1.0)
		sound.play()

	def jumpSound(self):
		sound = pygame.mixer.Sound(self.jump)
		sound.set_volume(1.0)
		sound.play()

	def growingSound(self):
		sound = pygame.mixer.Sound(self.growing)
		sound.set_volume(1.0)
		sound.play()

	
