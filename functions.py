import os
from const import *
import numpy
import random

def rename():
  folder = 'assets/sounds/hits'
  for filename in os.listdir(folder):
    infilename = os.path.join(folder,filename)
    if not os.path.isfile(infilename): continue
    oldbase = os.path.splitext(filename)
    newname = infilename.replace('.mp3', '.wav')
    output = os.rename(infilename, newname)

def load_PNG_Images(path, scale = 1):
  "load all file png in given path"
  lst=[]
  for filename in os.listdir(path):
    if filename.endswith(".png"):
      image = pygame.image.load(path + filename)

      newWidth = int(image.get_rect().width * scale)
      newHeight = int(image.get_rect().height * scale)

      resizeImage = pygame.transform.scale(image, (newWidth, newHeight))
      lst.append(resizeImage)

  return lst

