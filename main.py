import sys, pygame
import time
import datetime
import colors
from Text import *
from Background import *
from functions import *
from const import *
from Mario import *
from BlueTurtle import *
from BlueFlyTurtle import *
from Mushroom import *
from LongPipe import *
from Obj import SpriteGroupCoin, Coin
from Monster import SpriteGroupMonster
from Sounds import *
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d, %d" % (150, 30)

pygame.init()

background = Background()

clock = pygame.time.Clock()

screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption("Mario - Top1Pubg")

def newGameScreen():
    while True:
        bg = pygame.image.load("assets/images/menu/menu.png")
        screen.blit(bg, (0,0))
        Text.showTextMenu('MARIO', colors.Black, screen, WINDOW_WIDTH / 2 , WINDOW_HEIGHT / 4)
        startBtn = Text.showTextMenu('Start', colors.Black, screen, WINDOW_WIDTH / 2 , WINDOW_HEIGHT / 4 + 50)
        exitBtn = Text.showTextMenu('Exit', colors.Black, screen, WINDOW_WIDTH / 2 , WINDOW_HEIGHT / 4 + 100)

        for event in pygame.event.get():
            # QUIT
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    return
            #direct
            if event.type == pygame.MOUSEBUTTONDOWN:
                (x, y) = pygame.mouse.get_pos()
                if startBtn.collidepoint(x, y):
                    return
                if exitBtn.collidepoint(x, y):
                    sys.exit()

        pygame.display.update()
        clock.tick(FPS)
def playAgainScreen():
    while True:
        bg = pygame.image.load("assets/images/menu/menu.png")
        screen.blit(bg, (0,0))
        Text.showTextMenu('Play again?', colors.Black, screen, WINDOW_WIDTH / 2 , WINDOW_HEIGHT / 4)
        startBtn = Text.showTextMenu('Yes', colors.Black, screen, WINDOW_WIDTH / 2 , WINDOW_HEIGHT / 4 + 50)
        exitBtn = Text.showTextMenu('No', colors.Black, screen, WINDOW_WIDTH / 2 , WINDOW_HEIGHT / 4 + 100)

        for event in pygame.event.get():
            # QUIT
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            #direct
            if event.type == pygame.MOUSEBUTTONDOWN:
                (x, y) = pygame.mouse.get_pos()
                if startBtn.collidepoint(x, y):
                    return True
                if exitBtn.collidepoint(x, y):
                    return False

        pygame.display.update()
        clock.tick(FPS)

def main():
    #sound object
    sound = Sounds()
    sound.bgMusic()

    spriteMario = pygame.sprite.Group(MARIO)
    Mushrooms = []
    for i in range(20):
        Mushrooms.append(Mushroom( pow(i,2)*600 , 455))
    spriteMushRooms = []
    for i in Mushrooms:
        spriteMushRooms.append(pygame.sprite.Group(i))

    spriteGroupCoin = pygame.sprite.Group()

    groundGroup = pygame.sprite.Group(GroundSprite(0, WINDOW_HEIGHT - BACKGROUND_HEIGHT))
    skyGroup = pygame.sprite.Group(SkySprite(0, 0))

    blueTirtles = []
    blueTirtlePosX = [650, 700, 1000, 1402, 1555, 1800, 1850, 1930, 2000, 2888]
    for posX in blueTirtlePosX:
        blueTirtles.append(BlueTurtle(posX, 450))

    blueTirtleSprites = []

    for blueTirtle in blueTirtles:
        blueTirtleSprites.append(pygame.sprite.Group(blueTirtle))

    # blueFlyTirtle = BlueFlyTurtle(WINDOW_WIDTH + 100, 350)
    # blueFlyTirtleSprite = pygame.sprite.Group(blueFlyTirtle)

    pipes = []
    pipePosX = [900, 1953, 2700, 3501, 4533,  5555, 6213, 8102, 9999, 1201]
    for posX in pipePosX:
      pipes.append(LongPipe(posX, 380))
    pipeSprites = []
    for pipe in pipes:
        pipeSprites.append(pygame.sprite.Group(pipe))

    # setting()
    isPause = False
    while not isPause:
        screen.blit(background, (0, 0))
        if isPause is False :
            for event in pygame.event.get():
                # QUIT
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()


                # Key Down
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_p:
                        sys.exit()

                    if event.key == pygame.K_s:
                        pass
                    elif event.key == pygame.K_d:
                        pass
                    elif event.key == pygame.K_SPACE:
                        MARIO.jump()
                    elif event.key == pygame.K_UP:
                        MARIO.jump()

                if MARIO.heart <= 0 :
                    playAgainScreen()
                    MARIO.heart = 3
                # Key Up
                if event.type == pygame.KEYUP:
                    if MARIO.state != State.JUMPING:
                        MARIO.setState(State.FREE)

                # Mouse button down
                if event.type == pygame.MOUSEBUTTONDOWN:
                    (x, y) = pygame.mouse.get_pos()


            updateBg(groundGroup, skyGroup)
            MARIO.handle()

            MUSHROOM.handle()
            for i in Mushrooms:
                i.handle()
            for blueTirtle in blueTirtles:
                blueTirtle.handle(pipes, blueTirtles)
            # blueFlyTirtle.handle()
            for pipe in pipes:
                pipe.handle(pipes)

            SpriteGroupCoin.handle()
            SpriteGroupMonster.handle()

            MARIO.update()
            MUSHROOM.update()
            groundGroup.update(pipes)
            skyGroup.update(pipes)

            for i in spriteMushRooms:
                i.update()

            for blueTirtleSprite in blueTirtleSprites:
                blueTirtleSprite.update()
            # blueFlyTirtleSprite.update()
            for pipeSprite in pipeSprites:
                pipeSprite.update()
            SpriteGroupCoin.update()
            SpriteGroupMonster.update()

            skyGroup.draw(screen)
            groundGroup.draw(screen)

            SpriteGroupMonster.draw(screen)
            blueTirtleSprite.draw(screen)
            for i in spriteMushRooms:
                i.draw(screen)
            for blueTirtleSprite in blueTirtleSprites:
                blueTirtleSprite.draw(screen)
            # blueFlyTirtleSprite.draw(screen)
            for pipeSprite in pipeSprites:
                pipeSprite.draw(screen)

            spriteMario.draw(screen)
            SpriteGroupCoin.draw(screen)

            renderScoreTable()

            Text.showTextMenu('your score:', colors.Black, screen, 100, 50)
            Text.showTextMenu(str(MARIO.gold), colors.Black, screen, 100, 100)
        pygame.display.update()
        clock.tick(FPS)

def updateBg(groundGroup: pygame.sprite.Group, skyGroup:pygame.sprite.Group):

    lst = groundGroup.sprites()
    curSprite = lst[len(lst)-1]
    if (curSprite.position[0] + WINDOW_WIDTH < WINDOW_WIDTH):
        groundGroup.add(GroundSprite(curSprite.position[0] + WINDOW_WIDTH, WINDOW_HEIGHT - BACKGROUND_HEIGHT))

    lst = skyGroup.sprites()
    curSprite = lst[len(lst)-1]
    if(curSprite.position[0] + WINDOW_WIDTH < WINDOW_WIDTH):
        skyGroup.add(SkySprite(curSprite.position[0] + WINDOW_WIDTH,0))

def renderScoreTable():
    Text.showTextScoreTable('Your Coin:', colors.Black, screen, 100)
    Text.showTextScoreTable(str(MARIO.gold), colors.Black, screen, 250)

if __name__ == '__main__':
    newGameScreen()
    main()
    # playAgainScreen()
