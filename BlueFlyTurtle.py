import pygame

from const import *
from functions import *

SIZEX = 60
SIZEY = 80
DISTANCE_NEW_FRAME = 1

PATH = "assets/images/BlueFlyTurtle/"

class BlueFlyTurtle(pygame.sprite.Sprite):
    def __init__(self, centerx, centery):
        super(BlueFlyTurtle, self).__init__()

        # Initialize sprites
        self.sprites = {
            LEFT: [],
            RIGHT: []
        }
        # Load sprites
        loadImages(self.sprites[LEFT], PATH + "left/")
        loadImages(self.sprites[RIGHT], PATH + "right/")

        self.rect = pygame.Rect(centerx - SIZEX / 2, centery - SIZEY / 2, SIZEX, SIZEY)
        self.controlRect = getControlRect(self.rect.centerx, self.rect.centery)
        self.index = 1;
        self.direction = LEFT
        images = self.sprites[self.direction]
        self.image = images[self.index]
        self.totalDistance = 0

    def update(self):
        self.controlRect = getControlRect(self.rect.centerx, self.rect.centery)

    def move(self, direction):
        images = self.sprites[direction]
        if self.totalDistance > DISTANCE_NEW_FRAME:
            self.image = images[self.index]
            self.totalDistance = 0
            self.index += 1

        if self.index >= len(images):
            self.index = 0


        self.totalDistance += 1

    def handle(self):
        self.rect.centerx -= SPEED_DEFAULT
        self.move(LEFT)

def loadImages(list, path):
    for filename in os.listdir(path):
        if filename.endswith(".png"):
            image = pygame.image.load(path + filename)
            resizeImage = pygame.transform.scale(image, (SIZEX, SIZEY))
            list.append(resizeImage)

def getControlRect(centerx, centery):
    return pygame.Rect(centerx - SIZEX / 4, centery, SIZEX / 2, SIZEY / 2)
